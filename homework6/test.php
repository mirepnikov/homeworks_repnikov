
<?php
error_reporting(E_ALL);

$test_array = json_decode(file_get_contents('files/test.json'),true);

$test_id = $_GET['id'];

foreach ($test_array as $test)
{
    if ($test['id'] == $test_id)
    {
        $question = $test['question'];
        $answer = mb_strtolower($test['answer']);
    }
}

if (isset($_POST['answer']))
{
    $user_answer = mb_strtolower(htmlspecialchars($_POST['answer']));
    if ($user_answer == $answer)
    {
        $result = "Правильно!";
    }
    else
    {
        $result = "Неправильно!";
    }
}

?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Вопрос №<?=$test_id ?></title>
</head>
<body>

Внимание вопрос <br><br>

<form method="post">
    <p><?= $question ?> </p>
    <p><input type="text" name="answer" /></p>
    <?php if ($result!=null) { echo "<p>" . $result . "</p>"; } ?>
    <p><input type="submit" value="Отправить" /></p>
</form>

</body>
</html>



