
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Отправка теста на сервер</title>
</head>
<body>
<form enctype="multipart/form-data" method="post">
    <p>
        <label for="file_form">Выберете файл .json с тестами для загрузки:</label>
        <br>
        <input type="file" name="test" id="file_form">
        <br><br>
        <input type="submit" value="Отправить">
    </p>
</form>
</body>
</html>


<?php

error_reporting(E_ALL);

if (isset($_FILES['test'])) {

    $destinationFile = realpath(__DIR__ . "/files") . '/' . "test.json";


    if (move_uploaded_file($_FILES['test']['tmp_name'], $destinationFile)) {
        echo "Тесты загружены!";
    } else {
        echo "Что-то пошло не так";
    }

}

?>