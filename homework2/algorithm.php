<?php

error_reporting(E_ALL);

function generateNumber()
{

    $computerNumber = rand(0,10);
    $fp = fopen("number.txt", "a"); // Открываем файл в режиме записи
    ftruncate($fp, 0); //Очищаем файл
    $test = fwrite($fp, $computerNumber); // Записываем загаданное число
    if (!$test) echo 'Ошибка при записи в файл.';
    fclose($fp); //Закрытие файла

}

// Генерируем загаданное компьютером число

if ($_GET['new']==1)
{
generateNumber();
}

// Проверяем, есть ли число в файле, и генерируем, если нет
$fp = file("number.txt");
if (count($fp)==0) {
    generateNumber();
}

// Открываем файл, чтобы посмотреть, какое число было загадано компютером
$fp = fopen("number.txt", "r"); // Открываем файл в режиме чтения
if ($fp)
{
    $computerNumber=fgets($fp);
}
else echo "Ошибка при открытии файла";
fclose($fp);


$playerNumber = $_POST['playerNumber'];


// Определяем много или мало
if ($playerNumber<>null)
if ($playerNumber>$computerNumber) {$result="Много!";}
else if ($playerNumber<$computerNumber) {$result="Мало!";}
else if ($playerNumber===$computerNumber) {
    $result="Угадал! Я загадал новое";
    generateNumber(); // Генерируем новое число
}

?>

<html>
<head>
    <meta charset="utf-8">
    <title>Играем в игру</title>
</head>
<body>


<form action = "algorithm.php" method = "post">
    <p><strong>Я загадал число от 1 до 10, угадаешь какое?</strong></p>
    <p><input type="text" name="playerNumber" size="15" maxlength="25" value=""></p>
    <p><input type="submit" value="Играть"></p>

</form>

<a href = algorithm.php?new=1>Придумай другое!</a>

<br><br>

<?= $result ?>


</body>
</html>
