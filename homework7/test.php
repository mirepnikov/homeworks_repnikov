<?php

error_reporting(E_ALL);

$test_id = $_GET['id'];
$test_array = json_decode(file_get_contents('files/test.json'),true);

$test_exists = false;


foreach ($test_array as $test)
{
    if ($test['id'] == $test_id)
    {
        $test_exists = true;
        $question = $test['question'];
        $answer = mb_strtolower($test['answer']);
    }
}

if (!$test_exists) {
    header("HTTP/1.0 404 Not Found");
    echo "Тест не найден";
    die();
}

if (isset($_POST['answer']))
{
    $user_answer = mb_strtolower(htmlspecialchars($_POST['answer']));
    if ($user_answer == $answer)
    {
        $result = "Зачет!";
    }
    else
    {
        $result = "Незачет!";
    }
}
?>


<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Вопрос №<?=$test_id ?></title>
</head>
<body>

Внимание вопрос <br><br>

<form method="post">
    <p><b><?= $question ?></b></p>

    <label for="answer">Ваш ответ:</label>
    <p><input type="text" name="answer" /></p>

    <label for="name">Ваше имя:</label>
    <p><input type="text" name="name" /></p>

    <?php if ($result!=null)
    {
        echo "<p>" . $result . "</p>";

        if (isset($_POST['name']))
        {
            $name = $_POST['name'];
            echo '<p> <a href="get_certificate.php?mark=' . $result . '&name=' . $name . '"> Получить сертификат</a></p>';
        }

    }
    ?>
    <p><input type="submit" value="Отправить" /></p>
</form>

</body>
</html>