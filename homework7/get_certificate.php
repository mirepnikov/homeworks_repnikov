<?php

$text = $_GET['name'] . " – " . $_GET['mark'];

header('Content-Type: image/png');
header('Content-Disposition: attachment; filename="Certificate.png"');
$im = imagecreatetruecolor(800, 600);
$bc = imagecolorallocate($im, 255, 224, 221);
$textColor = imagecolorallocate($im, 133, 14, 91);
$boxPath = realpath(__DIR__ . '/assets/diploma.png');
$box = imagecreatefrompng($boxPath);
$fontPath = realpath(__DIR__ . '/assets/font.ttf');
imagefill($im, 0, 0, $bc);
imagettftext($im, 40, 0, 50, 250, $textColor, $fontPath, $text);
imagecopy($im, $box, 0, 340, 0, 0, 256, 256);
imagepng($im);
imagedestroy($im);
imagedestroy($box);
