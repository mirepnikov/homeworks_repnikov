<?php


error_reporting(E_ALL);


function makeThumbnails($dir, $thumb_dir)
{
    $thumbnail_width = 150;
    $thumbnail_height = 150;
    $thumb_beforeword = "thumb";
    $arr_image_details = getimagesize($dir); // pass id to thumb name
    $original_width = $arr_image_details[0];
    $original_height = $arr_image_details[1];
    if ($original_width > $original_height) {
        $new_width = $thumbnail_width;
        $new_height = intval($original_height * $new_width / $original_width);
    } else {
        $new_height = $thumbnail_height;
        $new_width = intval($original_width * $new_height / $original_height);
    }
    $dest_x = intval(($thumbnail_width - $new_width) / 2);
    $dest_y = intval(($thumbnail_height - $new_height) / 2);
    if ($arr_image_details[2] == IMAGETYPE_GIF) {
        $imgt = "ImageGIF";
        $imgcreatefrom = "ImageCreateFromGIF";
    }
    if ($arr_image_details[2] == IMAGETYPE_JPEG) {
        $imgt = "ImageJPEG";
        $imgcreatefrom = "ImageCreateFromJPEG";
    }
    if ($arr_image_details[2] == IMAGETYPE_PNG) {
        $imgt = "ImagePNG";
        $imgcreatefrom = "ImageCreateFromPNG";
    }
    if ($imgt) {
        $old_image = $imgcreatefrom($dir);
        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
        imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
        $imgt($new_image, $thumb_dir);
    }
}


$i=1;
$image_path = "img/" . $i . ".jpg";

$image_list = fopen('image_list.csv', 'w');
file_put_contents($image_list, '');

while (file_exists($image_path))
{

    $image_thumb_path = "img/" . $i . "_thumb.jpg";
    makeThumbnails($image_path, $image_thumb_path);

    $file_info = array($image_path, filesize($image_path), date("F d Y H:i:s.", filemtime($image_path)), $image_thumb_path);

    fputcsv($image_list, $file_info);

    //file_put_contents('image_list.csv', $file_info, FILE_APPEND);

    $i++;
    $image_path = "img/" . $i . ".jpg";
}

fclose($image_list);


?>