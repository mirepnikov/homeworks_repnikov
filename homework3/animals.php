
<html>

<head>

    <title>Жестокое обращение с животными</title>

</head>

<body>

<?php

error_reporting(E_ALL);


// Объявляем первоначальный массив животных

$animals = array("Africa" => array("Connochaetes", "Troglodytes gorilla", "Giraffa camelopardalis", "Hippotigris"), "South America" => array("Psittacidae", "Eunectes murinus"), "North America" => array("Mammuthus columbi", "Ursus arctos horribilis", "Alligator mississipiensis"), "Australia" => array("Macropus rufus", "Didelphis ursina", "Zaglossus bruijni", "Phascolarctos cinereus"), "Eurasia" => array("Alces alces", "Canis lupus", "Castor fiber", "Desmana moschata", "Alopex lagopus"));

//var_dump($animals); echo "<br><br>";

// Получаем животных в 2 слова

$animals_2_words = array();

foreach($animals as $continent => $animals_in_continent)
{
    foreach ($animals_in_continent as $animal)
    {
        $animal_words =  explode (" ",$animal);
        if (count($animal_words)==2)
        {
            $animals_2_words[] = $animal;
        }
    }
}

// Перемешиваем слова животных

// Находим массивы первых и вторых слов
foreach($animals_2_words as $animal)
{
    $animal_words = explode (" ",$animal);
    $animal_first_words[] = $animal_words[0];
    $animal_second_words[] = $animal_words[1];
}



// Перемешиваем массивы первых и вторых слов
shuffle($animal_first_words);
shuffle($animal_second_words);



// Делаем пермешанный массив животных
for($i=0; $i<count($animal_first_words); $i++)
{
    $animals_2_words[$i] = $animal_first_words[$i] . " " . $animal_second_words[$i];
}


//var_dump($animals_2_words);


// Определяем континенты животных

foreach($animals as $continent => $animals_in_continent)
{
    foreach($animals_in_continent as $animal_sample)
    {
        foreach ($animals_2_words as $animal)
        {

            $animal_words = explode (" ",$animal);
            if (strpos($animal_sample, $animal_words[0]) === 0)
            {
                $animals_2_words_in_continent[] = $animal;
            }
        }
    }
    $animals_2_words_in_continents[$continent] = $animals_2_words_in_continent;
    $animals_2_words_in_continent = array();

}

//echo "<br><br>";
//var_dump($animals_2_words_in_continents);



// Водим итоговый массив

foreach ($animals_2_words_in_continents as $continent => $animals_2_words_in_continent)
{
    echo "<h2>" . $continent . "</h2>";
    for($i=0;$i<count($animals_2_words_in_continent);$i++)
    {
        echo $animals_2_words_in_continent[$i];
        if ($i<count($animals_2_words_in_continent)-1)
        {
            echo ", ";
        }
    }
}

?>

</body>
</html>